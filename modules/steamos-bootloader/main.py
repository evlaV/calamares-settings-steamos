#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# === This file is part of Calamares - <https://github.com/calamares> ===
#
#   Copyright 2014, Aurélien Gâteau <agateau@kde.org>
#   Copyright 2014, Anke Boersma <demm@kaosx.us>
#   Copyright 2014, Daniel Hillenbrand <codeworkx@bbqlinux.org>
#   Copyright 2014, Benjamin Vaudour <benjamin.vaudour@yahoo.fr>
#   Copyright 2014, Kevin Kofler <kevin.kofler@chello.at>
#   Copyright 2015-2018, Philip Mueller <philm@manjaro.org>
#   Copyright 2016-2017, Teo Mrnjavac <teo@kde.org>
#   Copyright 2017, Alf Gaida <agaida@siduction.org>
#   Copyright 2017-2019, Adriaan de Groot <groot@kde.org>
#   Copyright 2017, Gabriel Craciunescu <crazy@frugalware.org>
#   Copyright 2017, Ben Green <Bezzy1999@hotmail.com>
#   Copyright 2019-2020, Collabora Ltd <arnaud.ferraris@collabora.com>
#
#   Calamares is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Calamares is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Calamares. If not, see <http://www.gnu.org/licenses/>.

import libcalamares
import os
import subprocess

from libcalamares.utils import check_target_env_call

import gettext
_ = gettext.translation("calamares-python",
                        localedir=libcalamares.utils.gettext_path(),
                        languages=libcalamares.utils.gettext_languages(),
                        fallback=True).gettext

def pretty_name():
    return _("Installing SteamOS bootloader.")

def efi_label():
    branding = libcalamares.globalstorage.value("branding")
    efi_bootloader_id = branding["bootloaderEntryName"]
    file_name_sanitizer = str.maketrans(" /", "_-")
    return efi_bootloader_id.translate(file_name_sanitizer)

def vfat_lookup(parent, name):
    '''
    VFAT can be tricky, as it is case-insensitive, but stores filenames in a
    case-sensitive way.
    This function returns a filename with the proper case in order to avoid errors
    '''
    for candidate in os.listdir(parent):
        if name.lower() == candidate.lower():
            return os.path.join(parent, candidate)
    return os.path.join(parent, name)

def run():
    efidev = str()
    partitions = libcalamares.globalstorage.value("partitions")
    rootpath = libcalamares.globalstorage.value("rootMountPoint")
    bootloader_name = efi_label()

    # Identify important devices
    for part in partitions:
        if part["mountPoint"] == "/efi":
            efidev = os.path.realpath(part["device"])

    libcalamares.utils.debug("Installing to " + efidev)

    # Find out GRUB directory path relative to the host system
    target_efi = os.path.join(rootpath, "efi")
    target_efi_dir = vfat_lookup(target_efi, "EFI")
    if not os.path.exists(target_efi_dir):
        os.makedirs(target_efi_dir)
    target_grub_dir = vfat_lookup(target_efi_dir, bootloader_name.lower())
    if not os.path.exists(target_grub_dir):
        os.makedirs(target_grub_dir)

    # Create the /efi/SteamOS directory
    target_steamos_dir = os.path.join(target_efi, bootloader_name)
    os.makedirs(target_steamos_dir)

    # Install chainloader
    check_target_env_call(["steamcl-install", "--flags", "restricted"])

    # Create partition definitions
    devices = []
    for part in partitions:
        # steamos partitions are the ones with a mountpoint
        if not part["mountPoint"]:
            continue
        devices.append(os.path.realpath(part["device"]))
    check_target_env_call(["steamos-partdefs", "--devices", " ".join(devices),
                           os.path.join("/efi", bootloader_name, "partitions")])

    # Generate a default SteamOS bootconf file
    ret = subprocess.run(["steamos-bootconf"], capture_output=True, text=True)
    if ret.returncode == 0:
        bootconf = open(vfat_lookup(target_steamos_dir, "bootconf"), "w")
        bootconf.write(ret.stdout)
        bootconf.close()

    # Check whether we are using an atomic and retrieve the value of 'roothash' if present
    cmdline = open("/proc/cmdline", "r")
    cmdargs = cmdline.read().split(" ")
    for line in cmdargs:
        if "roothash" in line:
            roothash_file = open(vfat_lookup(target_steamos_dir, "roothash"), "w")
            roothash_file.write(line.split("=")[1])
            roothash_file.close()
            break

    # Install GRUB bootloader and config
    check_target_env_call(["steamos-grub-mkimage"])
    check_target_env_call(["update-grub"])

    return None
