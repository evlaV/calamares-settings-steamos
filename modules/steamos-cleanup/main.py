#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# === This file is part of Calamares - <https://github.com/calamares> ===
#
#   Copyright 2019, Collabora Ltd <arnaud.ferraris@collabora.com>
#
#   Calamares is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Calamares is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Calamares. If not, see <http://www.gnu.org/licenses/>.

import libcalamares
import os
import shutil

import gettext
_ = gettext.translation("calamares-python",
                        localedir=libcalamares.utils.gettext_path(),
                        languages=libcalamares.utils.gettext_languages(),
                        fallback=True).gettext

def pretty_name():
    return _("Cleaning up.")

def run():
    root_mount_point = libcalamares.globalstorage.value("rootMountPoint")
    target_etc_dir = root_mount_point + "/etc/"
    target_ssh_dir = target_etc_dir + "ssh/"

    # Check whether we are using an atomic image and update the /etc location accordingly
    cmdline = open("/proc/cmdline", "r")
    cmdargs = cmdline.read().split(" ")
    readonly = False
    for line in cmdargs:
        if "roothash" in line:
            readonly = True
            break

    if readonly:
        target_etc_dir = root_mount_point + "/var/lib/overlays/etc/"
        target_ssh_dir = target_etc_dir + "upper/ssh/"

    # Clear the machine-id file on the installed system
    machine_id = open(target_etc_dir + "machine-id", "w")
    machine_id.close()

    # Remove SSH host keys on the installed system
    for file in os.listdir(target_ssh_dir):
        if file.startswith("ssh_host") and "key" in file:
            os.remove(target_ssh_dir + file)

    # Cleanup /var/boot, which contains dkms-rebuilt initrd with nvidia drivers.
    # For now, we prefer to discard it, to ensure that the first boot on the installed
    # steamos triggers a fresh dkms build of the nvidia drivers. Also, if we
    # wanted to keep /var/boot, TBH we'd need some more pain with grub-install.
    # I'm not ready for that yet...
    var_boot_dir = os.path.join(root_mount_point, "var", "boot")
    if os.path.exists(var_boot_dir):
        shutil.rmtree(var_boot_dir)

    return None
